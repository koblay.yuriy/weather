import React, {useEffect, useState, useMemo, useCallback} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Modal from 'react-bootstrap/Modal';
import {getData,getColor} from './initData';
import {usePosition} from './usePosition';
import './App.css';

const App = () => {
    const [text, setText] = useState(false);
    const [show, setShow] = useState(false);
    const [assent, setAssent] = useState(false)
    const [weather, setWeather] = useState({})
    const bgColor = useMemo(()=>getColor(weather?.current?.temp_c, 30,-10),[weather])
    // const {latitude,longitude, error} = usePosition();
    const [location, setLocation] = useState(false);
    const [error, setError] = useState(null);
    const onChange = position => {
        setLocation({
            latitude:position.coords.latitude,
            longitude: position.coords.longitude
        });
    };
    const onError = (error) => {
        setError(error.message);
    };
    useEffect(()=>{
        const geolocation = navigator.geolocation;

        if (!geolocation) {
            setError('Геолокация не доступна');
            return;
        }

        let getPosition = geolocation.watchPosition(onChange, onError);

        return () => geolocation.clearWatch(getPosition);

    },[])
    const handleClose = () => setShow(false);
    const setTemp = (e) => setWeather((pre)=>(
        {
            ...pre,
            current: {
                ...pre.current,
                temp_c:e.target.value
            }
        }
    ));
    const getAssent = () => {
        localStorage.setItem('loacationAssept', 1);
        setAssent(true)
        setShow(false)
    };
    const getLocationWeather = ()=>{
        getData(`http://api.weatherapi.com/v1/current.json?q=${text}&aqi=no`, '34046502ddec4150aae62610210609', setWeather);
    }
    useEffect(()=>{
        if(!!location && !localStorage.getItem('loacationAssept')) {
            console.log('!!latitude && !!longitude && !localStorage.getItem');
            setShow(true)
        }else{
            console.log('!!latitude && !!longitude && !localStorage.getItem --- false');
            setAssent(true)
        }
    },[location])

    useEffect(()=>{
        if(location && assent) getData(`http://api.weatherapi.com/v1/current.json?q=${location.latitude},${location.longitude}&aqi=no`, '34046502ddec4150aae62610210609', setWeather);
    },[assent,location])

    return (
        <div className="App" style={bgColor}>
            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Дозвіл</Modal.Title>
                </Modal.Header>
                <Modal.Body>Дозвольте використати ваше місце розташування</Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Ні
                    </Button>
                    <Button variant="primary" onClick={getAssent}>
                        Так
                    </Button>
                </Modal.Footer>
            </Modal>
           <div className="container d-flex  flex-column">
               <div className='d-flex justify-content-center align-items-center flex-column flex-grow-1'>
                   {!weather.error
                       ?
                           <div className='text-center'>
                               <img src={`http:${weather?.current?.condition?.icon}`} alt=""/>
                               <div className='text-center'>{weather?.current?.condition?.text}</div>
                               <div className='text-center'>temperature in {`${weather?.location?.name} ${weather?.current?.temp_c}`}&deg;C</div>
                           </div>
                       :
                           <div className='text-center'>
                              Некоретний запит
                           </div>
                   }

                   <div>
                       <Form.Control type="text" placeholder="Введіть місце" onChange={(e)=>{setText(e.target.value)}} />
                       <Button variant="primary" onClick={getLocationWeather}>
                           Отримати
                       </Button>
                   </div>
               </div>
               <input type="range" value={weather?.current?.temp_c} min='-10' max='30' onChange={setTemp} />
           </div>

        </div>
    );
}

export default App;
