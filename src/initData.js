export const getData = async (url,apiKey, setData)=>{
    const data = await fetch(`${url}&key=${apiKey}&lang=uk`);
    const res = await data.json();
    setData(res);
}
export const getColor = (i, maxT, minT)=>{
    let step = (maxT-minT)/100
    let getPersent = (Number(i)+10)/step;
    let r=0;
    let g=getPersent<=50 ? 255 : 247;
    let b=255;
    r=Math.round(getPersent<=50 ? (r+(255/100*getPersent))*2 : 255);
    g=Math.round(getPersent<=50 ? g-((g-247)/100*getPersent*2) : g-((g-140)/100*(getPersent-50)*2));
    b=Math.round(getPersent>=50 ? 0 : b-(b/100*getPersent*2));
    return{backgroundColor: `rgb(${r},${g},${b})`}
}
