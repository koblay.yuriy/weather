import { useState, useEffect } from 'react';

export const usePosition = () => {
    const [location, setLocation] = useState({});
    const [error, setError] = useState(null);

    const onChange = position => {
        console.log(position)
        setLocation({
            latitude:position.coords.latitude,
            longitude: position.coords.longitude
        });
    };
    const onError = (error) => {
        setError(error.message);
    };

    useEffect(()=>{
        const geolocation = navigator.geolocation;

        if (!geolocation) {
            setError('Геолокация не доступна');
            return;
        }

        let getPosition = geolocation.watchPosition(onChange, onError);

        return () => geolocation.clearWatch(getPosition);

    },[])

    return {...location, error};
}
